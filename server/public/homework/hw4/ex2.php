<?php

//@ author Nikita Evtefeev <nikita.evtefeev@gmail.com>

//Exercise 1
//while
function whileCount(array $arr): int
{
    $i = 0;
    while ($arr[$i]) {
        $i++;
    }
    return $i;
}

//do while
function dowhileCount(array $arr): int
{
    $i = -1;
    do {
        $i++;
    } while ($arr[$i]);
    return $i;
}


//for
function forCount(array $arr): int
{
    for ($i = 0; $arr[$i]; $i++) {
    }
    return $i;
}

//foreach
function foreachCount(array $arr): int
{
    $i = 0;
    foreach ($arr as $item) $i++;
    return $i;
}

$arr = [1, 2, 3, 4, 5];

echo whileCount($arr);
echo '<br>';
echo dowhileCount($arr);
echo '<br>';
echo forCount($arr);
echo '<br>';
echo foreachCount($arr);
echo '<br>';


echo '<br>';
echo '<br>';

//Exercise 2

function whileReverse(array $arr): array
{
    $res = [];
    $i = 0;
    $len = whileCount($arr) - 1;
    while ($arr[$i]) {
        $res[] = $arr[$len - $i];
        $i++;
    }
    return $res;
}

function dowhileReverse(array $arr): array
{
    $res = [];
    $i = 0;
    $len = whileCount($arr) - 1;
    do {
        $res[] = $arr[$len - $i];
        $i++;
    } while ($arr[$i]);
    return $res;
}

function forReverse(array $arr): array
{
    $res = [];

    $len = whileCount($arr) - 1;
    for ($i = 0; $arr[$i]; $i++) {
        $res[] = $arr[$len - $i];

    }
    return $res;
}

function foreachReverse(array $arr): array
{
    $res = [];
    $i = 0;
    $len = whileCount($arr) - 1;
    foreach ($arr as $key) {
        $res[] = $arr[$len - $i];
        $i++;
    }
    return $res;
}


$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

echo '<pre>';
print_r(whileReverse($arr));
print_r(dowhileReverse($arr));
print_r(forReverse($arr));
print_r(foreachReverse($arr));
echo '</pre>';

echo '<br>';
echo '<br>';

//Exercise 3
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
echo '<pre>';
print_r(whileReverse($arr));
print_r(dowhileReverse($arr));
print_r(forReverse($arr));
print_r(foreachReverse($arr));
echo '</pre>';

echo '<br>';
echo '<br>';
//Exercise 4


function strwhileReverse(string $str): string
{
    $res = '';
    $i = 0;
    $len = whileCount((array )$str) - 2;
    while ($str[$i]) {
        $res .= $str[$len - $i];
        $i++;
    }
    return $res;
}

function strdowhileReverse(string $str): string
{
    $res = '';
    $i = 0;
    $len = whileCount((array)$str) - 2;
    do {
        $res .= $str[$len - $i];
        $i++;
    } while ($str[$i]);
    return $res;
}

function strforReverse(string $str): string
{
    $res = '';

    $len = whileCount((array)$str) - 2;
    for ($i = 0; $str[$i]; $i++) {
        $res .= $str[$len - $i];

    }
    return $res;
}

//function strforeachReverse(string $str): string
//{
//    $res = '';
//    $i = 0;
//    $len = whileCount((array)$str) - 1;
//    foreach ($str as $key) {
//        $res .= $str[$len - $i];
//        $i++;
//    }
//    return $res;
//}

// foreach dont work with strings

$str = 'Hi I am ALex';

echo '<pre>';
print(strwhileReverse($str));
echo '<br>';
print(strdowhileReverse($str));
echo '<br>';
print(strforReverse($str));
echo '<br>';
//print(strforeachReverse($str));
echo '</pre>';


echo '<br>';
echo '<br>';

//Exercise 5

$str = 'Hi I am ALex';

echo strtolower($str);
echo '<br>';
echo '<br>';
//Exercise 6

$str = 'Hi I am ALex';

echo strtoupper($str);

echo '<br>';
echo '<br>';
//Exercise 7


$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

$i = 0;
$res0 = [];
while ($arr[$i]) {
    $res0[] = strtolower($arr[$i]);
    $i++;
}

$i = 0;
$res1 = [];
do {
    $res1[] = strtolower($arr[$i]);
    $i++;
} while ($arr[$i]);


$res2 = [];
for ($i = 0; $arr[$i]; $i++) {
    $res2[] = strtolower($arr[$i]);
}

$res3 = [];
foreach ($arr as $key) {
    $res3[] = strtolower($key);
}

echo '<pre>';
print_r($res0);
print_r($res1);
print_r($res2);
print_r($res3);
echo '</pre>';


echo '<br>';
echo '<br>';
//Exercise 8


$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

$i = 0;
$res0 = [];
while ($arr[$i]) {
    $res0[] = strtoupper($arr[$i]);
    $i++;
}

$i = 0;
$res1 = [];
do {
    $res1[] = strtoupper($arr[$i]);
    $i++;
} while ($arr[$i]);


$res2 = [];
for ($i = 0; $arr[$i]; $i++) {
    $res2[] = strtoupper($arr[$i]);
}

$res3 = [];
foreach ($arr as $key) {
    $res3[] = strtoupper($key);
}

echo '<pre>';
print_r($res0);
print_r($res1);
print_r($res2);
print_r($res3);
echo '</pre>';


echo '<br>';
echo '<br>';
//Exercise 9


$num = 1234678;

$str = (string)$num;
echo '<pre>';
print(strwhileReverse($str));
echo '<br>';
print(strdowhileReverse($str));
echo '<br>';
print(strforReverse($str));
echo '<br>';


//Exercise 10
function whileSort(array $arr): array
{
    $i = 0;
    while ($arr[$i]) {
        $ii = 0;
        while ($arr[$ii]) {
            if ($arr[$ii] < $arr[$ii + 1] && $arr[$ii + 1]) {
                $el = $arr[$ii + 1];
                $arr[$ii + 1] = $arr[$ii];
                $arr[$ii] = $el;
            }
            $ii++;
        }
        $i++;
    }
    return $arr;
}

function dowhileSort(array $arr): array
{
    $i = 0;
    do {
        $ii = 0;
        do {
            if ($arr[$ii] < $arr[$ii + 1] && $arr[$ii + 1]) {
                $el = $arr[$ii + 1];
                $arr[$ii + 1] = $arr[$ii];
                $arr[$ii] = $el;
            }
            $ii++;
        } while ($arr[$ii]);
        $i++;
    } while ($arr[$i]);
    return $arr;
}

function forSort(array $arr): array
{

    for ($i = 0; $arr[$i]; $i++) {

        for ($ii = 0; $arr[$ii]; $ii++) {
            if ($arr[$ii] < $arr[$ii + 1] && $arr[$ii + 1]) {
                $el = $arr[$ii + 1];
                $arr[$ii + 1] = $arr[$ii];
                $arr[$ii] = $el;
            }

        }

    }
    return $arr;
}

function foreachSort(array $arr): array
{

    foreach ($arr as $key) {

        foreach ($arr as $ii => $k) {
            if ($arr[$ii] < $arr[$ii + 1] && $arr[$ii + 1]) {
                $el = $arr[$ii + 1];
                $arr[$ii + 1] = $arr[$ii];
                $arr[$ii] = $el;
            }

        }

    }
    return $arr;
}


$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];

echo '<pre>';
print_r(whileSort($arr));
print_r(dowhileSort($arr));
print_r(forSort($arr));
print_r(foreachSort($arr));
echo '</pre>';


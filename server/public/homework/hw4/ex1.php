<?php
//@ author Nikita Evtefeev <nikita.evtefeev@gmail.com>


//Exercise 1

$i = 1000;
echo '<div style="overflow: auto; height: 100px; width: 100px; resize: vertical;">';
for ($n = 0; $n <= $i; $n++) {
    $c = 0;
    for ($nn = 1; $nn <= $n; $nn++) {
        if ($n % $nn == 0) $c++;

    }
    if ($c == 2) echo $n . '<br>';
}

echo "</div>";

echo "<br>";
echo "<br>";

//Exercise 2
$res = 0;
for ($i = 0; $i < 100; $i++) {
    $n = rand(0, 10000);
    if ($n % 2 == 0) $res++;
}

echo $res;


echo "<br>";
echo "<br>";

//Exercise 3
$res = [0, 0, 0, 0, 0];
for ($i = 0; $i < 100; $i++) {
    $n = rand(1, 5);
    $res[$n - 1]++;
}

foreach ($res as $i => $re) {
    echo ($i + 1) . ': ' . $re . '<br>';
}

echo "<br>";
echo "<br>";

//Exercise 4
$colors = ['red', 'green', 'blue'];
echo "<table border='1'>";
for ($i = 0; $i < 3; $i++) {
    echo "<tr>";
    for ($ii = 0; $ii < 5; $ii++) {
        echo "<td width='20px' height='20px' bgcolor='" . $colors[rand(0, 2)] . "'></td>";

    }

    echo "</tr>";
}

echo "</table>";


echo "<br>";
echo "<br>";

//Exercise 5

$month = rand(1, 12);

switch ($month) {
    case $month == 12 || $month <= 2:
        echo $month . " - Winter<br>";
        break;
    case $month <= 5:
        echo $month . " - Spring<br>";
        break;

    case $month <= 8:
        echo $month . " - Summer<br>";
        break;
    case $month <= 11:
        echo $month . " - Autumn<br>";
        break;
}


echo "<br>";
echo "<br>";

//Exercise 6

$string = 'airhfireooreifjireif';

if ($string[0] == 'a') echo 'yes';
else echo 'no';

echo "<br>";
echo "<br>";

//Exercise 7

$string = '321568435346565';

if ($string[0] == 1 || $string[0] == 2 || $string[0] == 3) echo 'yes';
else echo 'no';

echo "<br>";
echo "<br>";

//Exercise 8

$test = true;

echo $test ? 'True' : 'False';

echo '<br>';

if ($test) {
    echo 'True';
} else echo 'False';

echo '<br>';

$test = false;

echo $test ? 'True' : 'False';

echo '<br>';

if ($test) {
    echo 'True';
} else echo 'False';


echo "<br>";
echo "<br>";

//Exercise 9

$ru = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$en = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'];

$lang = 'en';

echo '<pre>';
if ($lang == 'ru') print_r($ru);
if ($lang == 'en') print_r($en);
echo '</pre>';

echo '<pre>';
print_r($lang == 'ru' ? $ru : $en);
echo '</pre>';

echo '<pre>';
print_r($$lang);
echo '</pre>';

echo "<br>";
echo "<br>";

//Exercise 10

$clock = rand(0, 59);
echo "$clock is a ";
if ($clock < 30) {
    if ($clock < 15) {
        echo '1 quarter of an hour';
    } else {
        echo '2 quarter of an hour';
    }
} else {
    if ($clock < 45) {
        echo '3 quarter of an hour';
    } else {
        echo '4 quarter of an hour';
    }
}

echo '<br>';

echo "$clock is a ";
echo ($clock < 30 ? ($clock < 15 ? '1' : '2') : ($clock < 45 ? '3' : '4')) . ' quarter of an hour';



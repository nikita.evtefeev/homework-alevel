<?php
//@ author Nikita Evtefeev <nikita.evtefeev@gmail.com>


//Exercise 1

//Вам нужно создать массив и заполнить его случайными
//числами от 1 до 100 (ф-я rand).
//Далее, вычислить произведение тех элементов,
//которые больше нуля и у которых четные индексы.
//После вывести на экран элементы,
//которые больше нуля и у которых нечетный индекс.

function exercise1(): bool
{
    $arr = [];
    for ($i = 0; $i <= 100; $i++) {
        $arr[] = rand(1, 100);
    }
    $num1 = 1;
    $num2 = [];
    foreach ($arr as $index => $item) {
        if ($item > 0 && $index % 2 == 0) $num1 *= $item;
        if ($item > 0 && $index % 2 == 1) $num2[] = $item;

    }
    echo $num1;
    echo '<br>';
    print_r($num2);
    return true;
}

exercise1();

echo '<br>';
echo '<br>';


//Exercise 2

//Даны два числа. Найти их сумму и произведение.
//Даны два числа. Найдите сумму их квадратов.

$exercise2sum1 = fn(int $a, int $b) => [$a + $b, $a * $b];
$exercise2sum2 = fn(int $a, int $b) => $a ** 2 + $b ** 2;

print_r($exercise2sum1(4, 6));
echo '<br>';
echo $exercise2sum2(6, 9);


echo '<br>';
echo '<br>';


//Exercise 3

//Даны три числа. Найдите их среднее арифметическое.

$exercise3 = fn($a, $b, $c) => ($a + $b + $c) / 3;

echo $exercise3(5, 4, 7);

echo '<br>';
echo '<br>';


//Exercise 4

//Дано число. Увеличьте его на 30%, на 120%.

$exercise430 = fn($a) => $a + $a * 0.3;
$exercise4120 = fn($a) => $a + $a * 1.2;

echo $exercise430(50);
echo '<br>';
echo $exercise4120(50);


echo '<br>';
echo '<br>';

//Exercise 5

//Пользователь выбирает из выпадающего списка страну
//(Турция, Египет или Италия), вводит количество дней
//для отдыха и указывает, есть ли у него скидка (чекбокс).
//Вывести стоимость отдыха, которая вычисляется как
//произведение количества дней на 400. Далее это число
//увеличивается на 10%, если выбран Египет, и на 12%,
//если выбрана Италия. И далее это число уменьшается на 5%,
//если указана скидка.

echo '<a href=form1.php>Задание 5</a>';

echo '<br>';
echo '<br>';

//Exercise 6

//Пользователь вводит свой имя, пароль, email.
//Если вся информация указана, то показать эти
//данные после фразы 'Регистрация прошла успешно',
//иначе сообщить какое из полей оказалось не заполненным.

echo '<a href=form2.php>Задание 6</a>';

echo '<br>';
echo '<br>';

//Exercise 7

//Выведите на экран n раз фразу "Silence is golden".
// Число n вводит пользователь на форме.
// Если n некорректно, вывести фразу "Bad n".

?>
    <form action="ex1.php" method="get">
        <label>
            Enter N<br>
            <input type="text" name="n">
        </label>
        <button type="submit">Show text</button>
    </form>
<?php

if ($_GET['n']) {
    $n = $_GET['n'];
    if (is_numeric($n) && $n > 0) {
        for ($i = 0; $i < $n; $i++) {
            echo "Silence is golden<br>";
        }
    } else {
        echo "Bad n";
    }
}


echo '<br>';
echo '<br>';

//Exercise 8

//Заполнить массив длины n нулями и единицами,
//при этом данные значения чередуются, начиная с нуля.


function exercise8(int $n): array
{
    $arr = [];
    $el = false;
    while ($n > 0) {
        $arr[] = (int)$el;
        $el = !$el;
        $n--;
    }
    return $arr;
}

print_r(exercise8(7));


echo '<br>';
echo '<br>';

//Exercise 9

//Определите, есть ли в массиве повторяющиеся элементы.

function exercise9(array $arr): bool
{
    $m = [];
    foreach ($arr as $i) {
        if (!$m[$i]) $m[$i] = true;
        else return true;
    }
    return false;
}

echo exercise9([8, 0, 0]) ? 'yes' : 'no';

echo '<br>';
echo '<br>';

//Exercise 10

//Найти минимальное и максимальное среди 3 чисел

$exercise10 = fn($a, $b, $c) => [$a < $b ? ($a < $c ? $a : ($b < $c ? $b : $c)) : $b,
    $a > $b ? ($a > $c ? $a : ($b > $c ? $b : $c)) : $b];

print_r($exercise10(54541, 77, 945454554));

echo '<br>';
echo '<br>';

//Exercise 11

//Найти площадь
$exercise11 = fn($a, $b) => $a * $b;

echo $exercise11(4, 5);

echo '<br>';
echo '<br>';
//Exercise 12

//Теорема Пифагора
$exercise12 = fn($a, $b) => sqrt($a ** 2 + $b ** 2);

echo $exercise12(4, 5);


echo '<br>';
echo '<br>';
//Exercise 13

//Найти периметр
$exercise13 = fn($a, $b) => $a * 2 + $b * 2;

echo $exercise13(4, 5);


echo '<br>';
echo '<br>';
//Exercise 14

//Найти дискриминант
$exercise14 = fn($a, $b, $c) => $b ** 2 - 4 * $a * $c;

echo $exercise14(4, 5, 6);


echo '<br>';
echo '<br>';
//Exercise 15

//Создать только четные числа до 100
function exercise15(): array
{
    $n = 1;
    $res = [];
    do {
        if ($n % 2 == 0) $res[] = $n;
        $n++;
    } while ($n <= 100);
    return $res;
}

print_r(exercise15());

echo '<br>';
echo '<br>';
//Exercise 16

//Создать не четные числа до 100
function exercise16(): array
{
    $n = 1;
    $res = [];
    do {
        if ($n % 2 != 0) $res[] = $n;
        $n++;
    } while ($n <= 100);
    return $res;
}

print_r(exercise16());



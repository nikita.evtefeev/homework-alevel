<?php

//Exercise 5

//Пользователь выбирает из выпадающего списка страну
//(Турция, Египет или Италия), вводит количество дней
//для отдыха и указывает, есть ли у него скидка (чекбокс).
//Вывести стоимость отдыха, которая вычисляется как
//произведение количества дней на 400. Далее это число
//увеличивается на 10%, если выбран Египет, и на 12%,
//если выбрана Италия. И далее это число уменьшается на 5%,
//если указана скидка.


if ($_POST) {
    $days = $_POST['days'];

    $sum = $days * 400;
    switch ($_POST['country']) {
        case 'e':
            $sum += $sum * 0.1;
            break;
        case 'i':
            $sum += $sum * 0.12;
            break;
    }
    if ($_POST['dis']) $sum -= $sum * 0.05;

    echo "Стоимость тура составит $sum\$";
}

?>

<form action="form1.php" method="post">
    <label>
        Выберите страну:
        <select name="country">
            <option value="t">Турция</option>
            <option value="e">Египет</option>
            <option value="i">Италия</option>
        </select>
    </label>
    <br>
    <br>
    <label>
        Сколько дней планируете отдыхать?
        <br>
        <br>
        <input type="number" name="days">
    </label>
    <br>
    <br>
    <label>
        У Вас есть скидка?
        <input type="checkbox" name="dis">
    </label>
    <br>
    <br>
    <button type="submit">Посчитать</button>
</form>


<?php
//@ author Nikita Evtefeev <nikita.evtefeev@gmail.com>

/**
 * return number in pow
 * @param integer $n
 * @param integer $s
 * @return integer
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
function myPow($n, $s): int
{
    $res = $n;
    for ($i = 1; $i < $s; $i++) {
        $res *= $n;
    }
    return $res;
}

echo myPow(2, 10);

/**
 * sort massive
 * @param array $arr
 * @return array
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 *
 */
function mySort(array $arr): array
{
    $c = func_num_args();
    foreach ($arr as $item) {
        foreach ($arr as $i => $el) {
            if ($c == 1) {
                if ($arr[$i + 1] && ($arr[$i] > $arr[$i + 1])) {
                    $el = $arr[$i + 1];
                    $arr[$i + 1] = $arr[$i];
                    $arr[$i] = $el;
                }
            } else {
                if ($arr[$i + 1] && ($arr[$i] < $arr[$i + 1])) {
                    $el = $arr[$i + 1];
                    $arr[$i + 1] = $arr[$i];
                    $arr[$i] = $el;
                }
            }

        }
    }

    return $arr;
}

$m = [];
$n = 20;
while ($n > 0) {
    $m[] = rand(1, 1000);
    $n--;
}
echo '<pre>';
myPrint(mySort($m));
echo '</pre>';
echo '<pre>';
myPrint(mySort($m, 'kkk'));
echo '</pre>';

/**
 * @param array   $arr
 * @param integer $find
 * @return integer
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 *
 */
function mySearch(array $arr, $find): int
{
    foreach ($arr as $i => $key) {
        if ($key == $find) {
            return $i;
        }
    }
    return -1;
}

$m = [4, 6, 8, 4, 2, 6, 8, 9];

echo mySearch($m, 8);
echo '<br>';

/**
 * my analog for print_r
 * @param array   $arr
 * @param integer $n
 * @return void
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 *
 */
function myPrint($arr, $n = 0): void
{
    foreach ($arr as $index => $item) {
        if (is_array($item)) {
            echo str_repeat("\t", $n) . $index . ' => Array<br>';
            myPrint($item, $n + 1);
        } else {
            echo str_repeat("\t", $n) . $index . ' => ' . $item . '<br>';
        }
    }
}


$m2 = [1, 2, 3, 4, 5, [6, 7, 8, 9, [0, 'test', 'user']], 8, [454, 75754, -65]];


echo '<pre>';
myPrint($m2);
//print_r($m2);
echo '</pre>';

<?php

$start = microtime(true);


/**
 *
 * return string
 */
function myTest(string $str): string
{
    return $str;

}


echo myTest('test2');

echo "test";


function calcNum(int $num): int
{
    return $num * $num;
}

$num = calcNum(78);

echo "<br>";

echo $num;


function getHello(string $name, string $surname = 'Ivanovich'): string
{

    return $name . ' ' . $surname;
}

echo "<br>";

$h = getHello('Nikita', 'Maksimovich');

echo $h;

echo "<br>";


function myecho()
{
    $count = func_num_args();
    for ($i = 0; $i < $count; $i++) {
        echo func_get_arg($i) . "<br>\n";
    }
}

myecho("test", "hello", "hink");
echo "<br>";


function myFn(...$array)
{
    print_r($array);
}


myFn("test", "hello", "hink");
echo "<br>";


$a = 5;
$b = $a;

$b = 0;

echo "a=$a, b=$b";
echo "<br>";

$arr = ['test', 'test2', 'test3'];
for ($i = 0; $i < 100; $i++) {
    $arr[] = $i;
}


//for ($i = 0; $i < 1000000; $i++) {
//
//}

//function myF2(&$array)
//{
//    for ($i = 0; $i < 100; $i++) {
//        $array[$i];
//    }
//}

//myF2($arr);

$time = microtime(true) - $start;
echo $time;
echo "<br>";
echo "<br>";


$A = ['a' => 'aaa', 'b' => 'bbb'];
$b = &$A['b'];    // теперь $b - то же, что и элемент с индексом 'b' массива
$b = 0;                // на самом деле $A['b']=0;
echo $A['b'];    // выводит 0

echo "<br>";
echo "<br>";


$a = 123;
$b = $a;
unset($a);

//var_dump($a);
var_dump($b);

echo "<br>";
echo "<br>";

function changeColor(&$my_color)
{            // теперь параметр будет ссылаться на оригинальное значение
    $my_color = 'синий';                // присваиваем новое значение
}

$color = 'красный';
changeColor($color);
echo $color;

echo "<br>";
echo "<br>";


$numWidgets = 10;
function &getNumWidgets()
{
    global $numWidgets;
    return $numWidgets;
}

$Numwidgetsref = &getNumWidgets();
$Numwidgetsref--;
echo " \$numWidgets = $numWidgets<br> ";                // Выведет " 9 ".
echo " \$numWidgetsRef = $Numwidgetsref<br> ";   // выводит " 9 ".

echo "<br>";
echo "<br>";

$var = 123;

function test(&$var)
{
    $var = 234;
}

test($var);
echo $var;
$a = 54;
global $a;


//echo "<pre>";
//
//print_r($GLOBALS);
//
//echo "</pre>";


echo "<br>";
echo "<br>";

function selfcount()
{
    static $count = 0;
    $count++;
    echo $count;
}

for ($i = 0; $i < 5; $i++) {
    selfcount();
}
selfcount();
selfcount();
selfcount();
echo "<br>";
echo "<br>";

$arr = ['rgrg', 'fefeegwg', 'herhrh', ['hththt', 'egrgrg']];

function showMassive($arr)
{
    foreach ($arr as $index => $item) {
        if (is_array($item)) showMassive($item);
        else echo "$index -> $item<br>";
    }
}

showMassive($arr);

echo "<br>";
echo "<br>";


function factor($n)
{
    static $count = 0;
    $count++;
    if ($n <= 0) {
        echo $count . ' static <br>';
        return 1;
    } else {
        return $n * factor($n - 1);
    }
}

echo factor(5);
echo "<br>";
print_r(20);


echo "<br>";
echo "<br>";


function dd(array $array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

$array = ['egege', 'rgrgrg', 'rgrgre'];

dd($array);
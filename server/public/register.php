<div class="container">
    <h1>Register to admin</h1>

    <form action="controller/controllerRegister.php" method="post">
        <div class="mb-3">
            <label class="form-label">First Name</label>
            <input type="text" class="form-control" aria-describedby="emailHelp" name="firstname">
        </div>
        <div class="mb-3">
            <label class="form-label">Last Name</label>
            <input type="text" class="form-control" aria-describedby="emailHelp" name="lastname">
        </div>
        <div class="mb-3">
            <label class="form-label">Email address</label>
            <input type="email" class="form-control" aria-describedby="emailHelp" name="email">
        </div>
        <div class="mb-3">
            <label class="form-label">Password</label>
            <input type="password" class="form-control" name="pass">
        </div>
        <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input">
            <label class="form-check-label">Check me out</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>


</div>
<!--<form action="page2.php" method="post">-->
<!--    <input type="text" name="login">-->
<!--    <input type="text" name="password">-->
<!--    <button type="submit">Send</button>-->
<!--    <button type="reset">Reset</button>-->
<!--</form>-->




<?php

//$t1 = microtime(true);
//$fp = fopen($_SERVER["DOCUMENT_ROOT"] . 'MyLog.txt', 'a+');
//for ($i = 0; $i < 100000; $i++) {
//
//    $date = date("Y-m-d G:i:s");
//
//    $str = $date . " " . print_r('test' . $i, true) . "\r\n";
//    fwrite($fp, $str);
//
//}
//fclose($fp);
//echo microtime(true) - $t1;

function declaration(): string
{
    return __FUNCTION__;
}

//echo declaration();


$expr = function () {
    echo "expr";
};

$expr();

$fni = fn() => 'Text';

echo $fni();


function add1($a, $b)
{
    return $a + $b;
}

$add2 = function ($a, $b) {
    return $a + $b;
};

$add3 = fn($a, $b) => $a + $b;


echo $add2(3, 4);
echo '<br>';
echo $add3(6, 4);


function f1($w, $h)
{
    return $w * $h;
}


$f2 = function ($w, $h) {
    return $w * $h;
};


$f3 = fn($h, $w) => $h * $w;

f1(5, 7);
echo '<br>';
$f2(5, 7);
echo '<br>';
$f3(5, 7);


function mmx1($a, $b, $c)
{
    return [$a < $b ? ($a < $c ? $a : ($b < $c ? $b : $c)) : $c,
        $a > $b ? ($a > $c ? $a : ($b > $c ? $b : $c)) : $c];

}

$mmx2 = function ($a, $b, $c) {
    return [$a < $b ? ($a < $c ? $a : ($b < $c ? $b : $c)) : $c,
        $a > $b ? ($a > $c ? $a : ($b > $c ? $b : $c)) : $c];
};

$mmx3 = fn($a, $b, $c) => [$a < $b ? ($a < $c ? $a : ($b < $c ? $b : $c)) : $c,
    $a > $b ? ($a > $c ? $a : ($b > $c ? $b : $c)) : $c];


echo '<br>';
echo '<pre>';
print_r(mmx1(9, 6, 4));
print_r($mmx2(1, 6, 4));
print_r($mmx3(9, 2, 4));
echo '</pre>';


$arr = ['rgregreg', 'aregregreh', 'aHTRhrth', 'R544trg'];

$res = array_filter($arr, function ($value) {
    if ($value[0] == 'a') {
        return $value;
    }

});

echo '<pre>';
print_r($res);
echo '</pre>';

function myFilter($arr, $fn)
{
    $res = [];
    foreach ($arr as $item) {
        if($fn($item)) $res[] = $fn($item);
    }
    return $res;
}

$res2 = myFilter($arr, function ($i) {
    if ($i[0] == 'a') {
        return $i;
    }
});


echo '<pre>';
print_r($res2);
echo '</pre>';

$res3 = array_map(function ($val){
    return strtoupper($val);
}, $arr);

echo '<pre>';
print_r($res3);
echo '</pre>';


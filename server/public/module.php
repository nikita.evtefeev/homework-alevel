<?php

/**
 * Exercise 1
 * Return number of directories from massive
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param array $a
 * @return int
 */
function calcDirsInMassive(array $a): int
{
    $n = 0;
    foreach ($a as $el) {
        $len = strlen($el);
        for ($i = 0; $i < $len; $i++) {
            if ($el[$i] == '/') {
                $n++;
                break;
            }
        }
    }
    return $n;
}

echo calcDirsInMassive(["C:/Projects/something.txt", "file.exe"]);
echo '<br>';
echo calcDirsInMassive(["brain-games.exe", "gendiff.sh", "task-manager.rb"]);
echo '<br>';
echo calcDirsInMassive(["C:/Users/JohnDoe/Music/Beethoven_5.mp3", "/usr/bin", "/var/www/myprojectt"]);

echo '<br>';
echo '<br>';


/**
 * Exercise 2
 * Check if you won in casino
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param array $a
 * @return bool
 */
function isYouWon(array $a): bool
{
    $pre = $a[0];
    foreach ($a as $el) {
        if ($pre == $el) {
            $pre = $el;
            continue;
        }
        return false;
    }
    return true;
}

echo isYouWon(["9919", "9919", "9919"]) ? 'True' : 'False';
echo '<br>';
echo isYouWon(["abc", "abc", "abb"]) ? 'True' : 'False';
echo '<br>';
echo isYouWon(["@", "@", "@"]) ? 'True' : 'False';
echo '<br>';


echo '<br>';
echo '<br>';


/**
 * Exercise 3
 * Help Frodo and Sam to be together
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param array $a
 * @return bool
 */
function isTogether(array $a): bool
{
    $p1 = 'Sam';
    $p2 = 'Frodo';
    $i1 = false;
    $i2 = false;
    foreach ($a as $key => $i) {
        if ($i == $p1 || $i == $p2) {
            if (!$i1) {
                $i1 = $key;
            } else {
                $i2 = $key;
            }

        }
    }
    if (abs($i1 - $i2) == 1) {
        return true;
    } else {
        return false;
    }
}

echo isTogether(["Sam", "Frodo", "Troll", "Balrog", "Human"]) ? 'True' : 'False';
echo '<br>';
echo isTogether(["Orc", "Frodo", "Treant", "Saruman", "Sam"]) ? 'True' : 'False';
echo '<br>';
echo isTogether(["Orc", "Sam", "Frodo", "Gandalf", "Legolas"]) ? 'True' : 'False';

echo '<br>';
echo '<br>';


/**
 * Exercise 4
 * Return the second max number from massive
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param array $a
 * @return int
 */
function getSecondMax(array $a): int
{
    $max = $a[0];
    $index = 0;
    foreach ($a as $key => $item) {
        if ($item > $max) {
            $max = $item;
            $index = $key;
        }

    }
    unset($a[$index]);
    $max2 = $a[0];
    foreach ($a as $item) {
        if ($item > $max2) {
            $max2 = $item;
        }

    }
    return $max2;
}


echo getSecondMax([1, 2, 3]);
echo '<br>';
echo getSecondMax([1, -2, 3]);
echo '<br>';
echo getSecondMax([0, 0, 10]);
echo '<br>';
echo getSecondMax([-1, -2, -3]);


echo '<br>';
echo '<br>';


/**
 * Exercise 5
 * Return strings that have max length
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param array $a
 * @return array
 */
function getStrings(array $a): array
{
    $len = strlen($a[0]);
    foreach ($a as $item) {
        $ilen = strlen($item);
        if ($len < $ilen) {
            $len = $ilen;
        }
    }
    $res = [];
    foreach ($a as $item) {
        if (strlen($item) == $len) {
            $res[] = $item;
        }
    }
    return $res;
}

echo '<pre>';
print_r(getStrings(["in", "Soviet", "Russia", "frontend", "programms", "you"]));
echo '<br>';
print_r(getStrings(["using", "clojure", "makes", "your", "life", "greater"]));
echo '<br>';
print_r(getStrings(["a", "b", "c", "d"]));
echo '</pre>';

echo '<br>';
echo '<br>';


/**
 * Exercise 6
 * Return student graduation
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param array $a
 * @return string
 */
function getGrade(array $a): string
{
    $sum = 0;
    foreach ($a as $i) {
        $sum += $i;
    }
    $gr = $sum / count($a);
    switch ($gr) {
        case $gr > 90:
            return "Grade: A";
        case $gr > 80:
            return "Grade: B";
        case $gr > 70:
            return "Grade: C";
        case $gr > 60:
            return "Grade: D";
        default:
            return "Grade: F";
    }
}


echo getGrade([90, 91, 99, 93, 100]);
echo '<br>';
echo getGrade([92, 77, 85, 84, 84]);
echo '<br>';
echo getGrade([70, 72, 78, 72, 70]);
echo '<br>';
echo getGrade([60, 61, 62, 63, 70]);
echo '<br>';
echo getGrade([50, 42, 20, 31, 0]);
echo '<br>';
echo getGrade([10, 9, 2, 3, 5]);


echo '<br>';
echo '<br>';


/**
 * Exercise 7
 * Return figure name
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param int $n
 * @return string
 */
function getFigure(int $n): string
{
    $figures = [
        "circle",
        "semi-circle",
        "triangle",
        "square",
        "pentagon",
        "hexagon",
        "heptagon",
        "octagon",
        "nonagon",
        "decagon"];

    return $figures[$n - 1];
}

$n = 1;
while ($n < 11) {
    echo getFigure($n);
    echo '<br>';
    $n++;
}


echo '<br>';
echo '<br>';

/**
 * Exercise 8
 * Help farmer to calculate legs
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param int $n1
 * @param int $n2
 * @param int $n3
 * @return int
 */
function getNumberLegs(int $n1, int $n2, int $n3): int
{
    return $n1 * 2 + $n2 * 4 + $n3 * 4;
}

echo getNumberLegs(2, 3, 5);
echo '<br>';
echo getNumberLegs(1, 2, 3);
echo '<br>';
echo getNumberLegs(5, 2, 8);


echo '<br>';
echo '<br>';


/**
 * Exercise 9
 * return massive of strings lengths
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param array $a
 * @return array
 */
function stringsToLengths(array $a): array
{
    $res = [];
    foreach ($a as $i) {
        $res[] = strlen($i);
    }
    return $res;
}

echo '<pre>';
print_r(stringsToLengths(["hello", "world"]));
echo '<br>';
print_r(stringsToLengths(["some", "test", "data", "strings"]));
echo '<br>';
print_r(stringsToLengths(["clojure"]));
echo '</pre>';


echo '<br>';
echo '<br>';

/**
 * Exercise 10
 * Return 'true' if word ends by 's'
 * @ author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 * @param string $s
 * @return bool
 */
function checkEnding(string $s): bool
{
    return $s[strlen($s) - 1] == 's';
}


echo checkEnding("fork") ? 'True' : 'False';
echo '<br>';
echo checkEnding("forks") ? 'True' : 'False';
echo '<br>';
echo checkEnding("clojure") ? 'True' : 'False';
echo '<br>';
echo checkEnding("bytes") ? 'True' : 'False';
echo '<br>';
echo checkEnding("test") ? 'True' : 'False';
